# Autor_ Angel Marcelo Merchan Infante
# Email_ angel.merchan@unl.edu.ec

# Instruccions_ You are given a dictionary consisting of word pairs.
# Every word is a synonym of the other word in its pair.
# All the words in the dictionary are different.

# Read a string:
# s = input()
# Print a value:
# print(s)

s = int(input())
dictionary = {}
t = list(input().split())
for i in range (s):
    dictionary[s[0]] = s[1]
    if i < s-1:
        t = list(input().split())
tex = input()
for k, v in dictionary.items():
    if k == tex:
        print(v)
    if v == tex:
        print(k)